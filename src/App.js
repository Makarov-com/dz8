import { useState, useEffect } from "react";
import './App.css';

function App() {
    const [theAlbumId, setAlbumId] = useState(0);
    const [albums, setAlbums] = useState([]);
    const [photos, setPhotos] = useState([]);


    const onClick = (albumId) => (e) => {
        setAlbumId(albumId);
    }

    useEffect(() => {
        fetch("https://jsonplaceholder.typicode.com/albums")
            .then((response) => response.json())
            .then((data) => {
                setAlbums(data);
            });
        fetch("https://jsonplaceholder.typicode.com/photos")
            .then((response) => response.json())
            .then((data) => {
                setPhotos(data);
            });
    });


    return (
        <div className="gallery">
            <aside className="albums">
                <h4 className="albums__title">Albums</h4>
                <ul className="albums-list">
                    {albums.map(({ id, title }) => {
                        let classes = "albums-list__item";

                        if (theAlbumId === id) {
                            classes += " albums-list__item--active";
                        }

                        return (
                            <li key={id} className={classes} onClick={onClick(id)}>{title}</li>
                        );

                    })}
                </ul>
            </aside>
            <main className="photos">
                {photos.map(({ url, thumbnailUrl, albumId }) => {
                    if (albumId === theAlbumId) {
                        return (
                            <a target="blank" href={url}><img src={thumbnailUrl} alt='' /></a>
                        )
                    }
                    return null

                })}
            </main>
        </div>
    );
}

export default App;
